# Stata Software Training

Regardless of which software you want to use, doing effective programming allows you not only to ensure that your results are what you intend, but also make it easy to communicate to others - whether it's your analyst, or other researchers in your field. The resources and reading below provide an introduction to basic use of the software: selecting and manipulating data, computing statistical information, and creating simple models of the data. They are by no means exhaustive (there are many options, packages, and plug-ins that can make your life easier) but should provide you with a good foundation to build your skills. In statistical programming there is no substitute for simply writing up analyses and running them against data. 

How should you learn using this resource? Create a folder for your learning and download the material from this repository into it (go to the main page: https://gitlab.com/c3754/stata_training and click on "Code" -- at the bottom of this menu will be the option to download it as a .zip file.) Code up the activities in each video along with the presenter and save it in your folder for reference. I've tried to choose videos where the dataset is available as an example file in the program (which is common). Where they are not, I have created a version of the dataset that you can use. Next, re-do the exercise by yourself trying not to look at the references or help-files. Repeat this several times until you can do it without checking. Learning how to do the core actions in the training without looking is important because, with few exceptions, it will be prohibitively slow to have to look up these actions every time you need them while conducting a real project. Next, realize that complicated syntax that is infrequently used is *not* to be memorized (there's a running joke in the Stata community that the reshape help-file is the second most-read English text behind the bible). Be aware of the functionality that exists in the software and know exactly how to use the documentation to implement infrequently used commands. If you happen to learn something by repetition it will happen organically. At the end of the module you will find a set of exercises that can be undertaken to test your ability with the core features. Try to do these without looking at any help-files until the last instruction which will be a test of how well you can use the help-files to do something you haven't seen before.

# Introduction
This content covers the basics of working with data in Stata. What is important to realize about Stata is that the point and click interface covers all the basic features of the software, but advanced functions aren't always available through the menus. When you use the point and click interface, the full list of commands can easily be exported to a ".do file" to keep a record as you'll see in the Stata Overview section. Stata is a paid software, but is almost certainly available through your university in a computer lab. Stata works through operations called "commands" which are routines that are called via a simplified syntax by the user. For example, the t-test command ttest takes a few arguments and does all the computations behind the scenes. What's behind the curtain can be found inside the ".ado" (auxiliary do) files that can be found on your machine -- usually in the installation folder. You don't need to worry about these now, but once you become a more advanced user it can be helpful to know where to dig into the core programming of the application.

Generally, I will follow text emphasis as follows: `plaintext like this` is an actual command or part of a command that you can type into Stata to do something. For blocks of commands I will use both 

> `indenting and plaintext.` 

"Text appearing in quotes" is either a specific reference like the name of a dataset or part of the software, or a placeholder for a file-path in your system. In the latter case I try to "*put asterices around the words in the quotes*". Other emphasis of text elements might be *italics* or **bold** text.

#### A tour of the Stata interface

Stata's user interface is made up of a series of "panes". By default, the "History" pane is on the left. The history pane stores all the commands you execute either via the point and click menus, or that you type into the command pane at the bottom. You can search the commands you've used, sort them by command or by "_rc" which stores codes that failed to execute (because of errors). It's also worth noting that these commands that failed due to an error appear in red in the history pane. The "#" column stores the *order* in which the commands were executed so you can go back to the history as it was originally generated. 

![History Pane](./images/stataimg1.png) 

Next is the "Result" pane (the big one in the middle). This pane can't be hidden like other panes can be, and is the place where all the commands and the information they calculate will be displayed. Any command that results in an error will also show up here in red text. 

![Result Pane](./images/stataimg2.png)

Underneath the result pane is the "Command" pane. Here is where you type the commands you want to do when working with Stata "interactively" (ie. typing and executing single lines or small pieces of code at a time). It makes sense to show you a most basic command now: For my demo window, I'm using the "voter.dta" file that is installed with Stata as an "example dataset" which are very useful for teaching others to use Stata. To load an example dataset we type "sysuse" then a space then the name of the dataset. In this case: type `sysuse voter` then press enter/return to execute the command. Once you type this your window will look like mine in image 1 (maybe with a different OS or version #, license info, and the setup file "Running C:\Program Files\Stata17\profile.do" - these differences are inconsequential for this exercise). You will also have only one command available in the history pane, but you can easily type two more commands `summarize frac` (then press enter/return) and `twoway inc` (then enter/return) to recreate my window as you see it above.

Moving on, our next pane is the top right "Variables" pane which contains all the variables in the current dataset as well as their "labels" -- an optional description of the variable provided by the dataset creator or, optionally, the user (we will cover labeling data when we deal with creating new variables). Like the history pane, there is also a search bar that will search both variable names and variable labels to help you navigate your data -- this one though you might actually make use of. 
	
![Variable Pane](./images/stataimg3.png)

Finally is the "Properties" pane. If this pane isn't visible, you can add it by clicking "window" then "properties" or pressing ctrl/cmd 5. This gives you information about the dataset. The key things are the location of the data in your system (under Filename), the number of observations and variables, and the sort order. If you haven't done anything yet, the "variables" section will be empty, but if you click on a variable in the variables pane (as I clicked on "pfrac") you will see what information is available for the variable (we'll talk about what some of these things mean in the next section). 

![Properties Pane](./images/stataimg4.png)

Now you have the basics of the user interface. It can be customized, which I won't get to other than briefly here, but the windows can be closed (if you need to re-add, use the "window" menu), resized, and moved around. The default colour scheme can be changed under "Edit/Preferences/General preferences" - those who like Dark mode will find a very good one here, but I do reference text colors on occasion throughout the guide -- these will refer to the standard default colour scheme. You should also note that very old versions of Stata did not have different colours (my recollection is that these were introduced in Stata 13.) If you have a very old version of Stata, many parts of this tutorial will be difficult (the example datasets for example might be different and those I provide won't load directly.) That said, Stata goes to heroic lengths to ensure that code and operations written in old versions do not fail when software is updated. Code written in Stata 1.0 from January 1985 can generally be run in Stata 18 released in 2023 without modification.

#### An overview of Stata

- [Stata Overview](https://www.youtube.com/watch?v=02qrJEblQwk)

Don't try to memorize this content, Chuck does an entire overview of the software in a few minutes. This is just a helpful tour which you should watch before we begin.

#### How to use help files in Stata

- [Quick help in Stata](https://www.youtube.com/watch?v=UpXNMeTzmuI)

Helpfiles are one of the biggest reasons to choose working within Stata over other software. The helpfiles are very thorough and have a great deal of quality control. The format is very standardized, and there are links throughout to help you navigate through the helpfile. Let's use the helpfile for `generate` to consider how helpfiles work (type `help gen' in the command window to bring it up in your system):

The first line after the title is *syntax*. Syntax describes how your line of code should look in order to work:

> `generate [type] newvar[:lblname]=exp [if] [in] [,before(varname) | after(varname)]` 

How do I read this? well to start with, anything inside square-brackets \[\] is optional. Anything outside of square-brackets isn't. So the only thing I need to type to make generate work is: `generate newvar=exp` I can specify as many or as few of the optional elements as I want. To specify an optional element, I just need to write the option in place of the place-holder in the syntax line. Sometimes, options are exclusive. Indeed, if I try to specify `before(varname)` and `after(varname)` in a generate command I will get an error (even if they are next to each other and it should work in theory). 

Some things you might see: the command itself will always be the first mandatory piece (the command may allow \[by\]) *varlist* - provide one or more variables; *varname* - provide only one variable; *newvar/newvarlist*- provide one (or more) variables that are not currently in your data; *=exp* - provide an expression. There are other things that might be there and you can learn what is expected further down in the helpfile. For `generate` we now look at the "Options" section which explains both how to specify `before` and `after`, and what they do. You'll see `nopromote` and `permanently` here which might not make sense, but recognize that `generate` shares a helpfile with `replace` and `set type`. You will also see, throughout the syntax, some characters underlined. This underlining indicates that an option or command can be abbreviated such that the command <ins>su</ins>mmarize, for example, only requires you to type `su` to initiate the command (the same works for options). 

Now the most helpful section for tricky commands -- "Examples". Here you'll find working syntax that can be emulated in your code. Each example might include a *setup* line that tells you how to get your Stata instance in a position to use the example. In newer versions of stata, the helpfiles sometimes have links to videos.

Lastly, the stata help file is an abbreviated version of what's in the .pdf manual. The link to the relevant entry in the manual is found at the very top of the help file. 

You may find it appropriate to view the helpfiles for the commands you learn throughout the rest of the material. To do this, simply type `help` followed by the name of the command so to access the helpfile for generate used as an example here, you typed `help generate` (or `help gen` since gen is an acceptable abbreviation per the helpfile)

# Basic Operations with Data
#### Opening data from Excel, SAS or text-data (.csv, .txt)

- [In Excel](https://www.youtube.com/watch?v=NEhz9t9o6WM)
- [From SAS & SPSS](https://www.youtube.com/watch?v=SoJPPZ0giZw)
- [From Text-data](https://www.youtube.com/watch?v=8vwRchxkaZs) most commonly ".csv" (comma separated value) data.

Once you've seen these videos, you'll know both how to add the data to stata using the menu, but also have seen the commands that can be typed directly. Generally these follow the format: `import *format* using "*filepath/filename*", clear` 

`clear` is available as an option to any open/import function and replaces the data in memory. If you don't specify clear, there will be an error unless you don't have a current dataset. If you do specify clear the data will be lost (so make sure you've saved it or can restore it if you still need it.)

The last thing to know is how to use data that is stored by Stata in the proprietary ".dta" format. Here, to use the menu we click "File/Open" and navigate to our dataset in Stata format. This will issue the command: `use "*filepath/filename*"`

Similarly, if we want to restrict the dataset that we're opening to either a subset of observations or a subset of the variables, we can choose "File/Open data subset" which brings up a dialog box that allows us to specify the variables we want to import. This will issue the command: `use *variables* *if* *in* using *"filepath/filename"*`

It behooves us now to briefly discuss data *formats* and how to look at data in Stata. There are two ways to bring up the data in your system. In the top banner you will see two icons

![Data browse & edit tab](./images/stataimg5.PNG)

The one on the right is the "data browser" which is the only one you should generally use unless you have a very specific reason not to. In the browser, the data are not editable which keeps you from accidentally making changes to your data while you're looking at it. The one on the left is the "data editor" which does allow for changes to be made -- even unintentional ones -- cell by cell which will feel a bit like a stripped down version of MS Excel.

Start by loading the example data "lifeexp" as you did with the voter data in the previous section (`sysuse lifeexp`). Now open the Data Browser. You should see this:
![Data Browser](./images/stataimg6.PNG)

I will point out a few things related to variable types that are critical to know because they determine how you can interact with your data. We will start with the "safewater" column. There are a number of numbers and a number of ".". This variable is a **Numeric** variable which means... it is a number. Practically though, it means that, you could do calculations using it or create new information using a mathematical function. In a numeric variable a "." means that the observation is missing (you might see .a or .b sometimes also). Missing variables will be excluded from most calculations, but not from logical operations (to be discussed later). There are 5 "formats" that numeric data can take: byte, int, long, float, double. These aren't that important for beginners, but the format in which a variable is generated affects how much space it takes up in memory. Bytes, integers and long are actually all integers, but take up different amounts of space because bytes have a maximum value of 100, integers max=32,740, and long max slightly over 2 billion (note that the minimum is the corresponding negative integer). Floats and doubles can store decimals with varying precision (floats store 7-digits of accuracy, and doubles 16). The implications aren't that important for now since Stata will generally decide how data should be stored relatively efficiently.

Let's turn to the "region" column. You will see that the text is in **blue**. This indicates that the variable is a *labeled* numeric variable. Indeed if you click on one of the cells containing "South America", you will see the number 3 at the top in the grey box, "North America" corresponds to 2 (as you see in the above screen capture), and "Europe & C.Asia" corresponds to 1. This is done both to minimize the storage cost of data, make it easier to use command modifiers and logical statements (presented in the next section), and to interpret output from calculated commands. Labels behave like a pair of glasses that the software filters the stored data through when displaying it to you. The actual value stored in the program for the value of "region" for an observation in North America is 2, but anytime you look at it whether in the data browser or if you display it in some way (like in a table for example) will be filtered through the label before being displayed. Note that you can do silly things with labeled numeric variables. For example you could put "region" into a regression and Stata would try to estimate the marginal effect on the outcome variable of a one-unit increase in region -- which of course makes no sense. You could also calculate the average region, which similarly would make very little sense. It is up to you to ensure that you are using the right types of operators on the data; Stata will let you do incorrect things.

Lastly, we turn to the column "country" these are colored brown. If you click on "Italy", you will see "Italy" in the top where you'll recall that the blue variable showed a number. As you may have guessed, the value shown in the box at the top is what is actually contained in the cell, stripped of all the labeling that may be applied. Without getting into any specifics about why the data are stored in these formats, there are 2 string types in Stata. str# stores strings up to # characters (which explains why Yugoslavia, FR (Serb./Mont.) is abbreviated the way it is as it must be 28 characters or fewer to fit in a str28 variable). Str# variables can contain information up to 2045 characters in length in an Str2045 variable. The next type is strL or Long strings which can contain 2-billion characters. strL variables are very costly in terms of memory. If you have a dataset that you think is stored inefficiently, try typing `compress` in the command window and Stata will automatically try to optimize storage without reducing the precision of your data. Note that operations on strings are very different in Stata. You can't accidentally calculate the average of "country" because it does not have a numeric interpretation.

#### Using command modifiers

- [Stata Command Modifiers--if, in, and by/bysort](https://www.youtube.com/watch?v=PMqYIytm68A) | [Sample data to be used with the video](./sample_data/command_modifiers.dta)

Parts of this video might seem a bit confusing, but we have to start somewhere. This content will start to make more sense as you get more familiar with working in Stata. Alan uses the `summarize` command which creates a summary table for a variable, and the `list` command which simply creates a table of values -- you'll learn more about these in the next section. When following along, you should type the text that Alan highlights and executes into the command window (he is using something called a ".do file" that will be covered later). To check your work against his commands with my fake data you can check the answers [here](./solutions/solution-command_modifiers_solution.smcl). This link provides a file in a format called "smcl" which mimics the look and feel of the Stata results window and should facilitate comparison. You'll need to download it and open it within Stata so that it displays correctly. Note also that since the data I made up are just random numbers, none of the qualitative conclusions that can be drawn from the real values in the video will apply.

#### Exploring data

- [Data Exploration](https://youtu.be/9a1DXX7EwjM?feature=shared)

The video covers what I see as the ways to explore the data sensibly. There are other ways to do things (including, for small datasets, using simple data visualizations, which may be the best way to understand your data). Evidently, you can also just open the data in browse mode as we went over in the "importing data" section. But this sort of unstructured exploration is largely a waste of time for anything other than quickly visualizing, by colour, the data types. What you may find useful in the data browser when investigating problems is to right click on a variable, choosing show only selected variables (as in the screenshot below) 

![Data Browser](./images/stataimg6.PNG)

Let's work through a concrete example. Start by loading up the nhanes data (a rather large dataset) `use https://www.stata-press.com/data/r17/nhanes2.dta`. Let's check two things: That the hsiz4 variable is properly calculated against hsizgp and that there are no errors in our data for bpsystol & bpdiast. Here's the steps to take

1. Open the data browser, right click hsizgp and choose "show only selected variables" 
2. Check off hsiz4 so that Stata displays only these two columns. (Recall that if you click on "Name" at the top of the variables pane, the variables will display alphabetically by name)
3. Now right click "Data" in the menu and choose "Sort" to bring up the dialogue box. You can choose a Standard sort (only sorts lowest to highest) or Advanced sort, choose advanced for this example. 
4. In the "Variables" box we put the name of the variables we want to sort. If we want them to go lowest to highest we put only the name of the variable, if we want to go highest to lowest we put a minus (-) before the variable name. We'll sort hsizgp from high to low and hsiz4 from low to high so that if there is a household of size 4 and a zero in hsiz4 (which should never happen) we see this as the first instance in sequence that we observe for hsiz4. Note that the order we type things matters. Type "-hsizgp hsiz4" and press OK. This sorts in descending order hsizgp then within equal values of that, sorts hsiz4 in ascending order. 
5. Scroll to line 1674 where the households in my data change size (if I hadn't pre-done this you would have to figure out which row the values change in). You can see that the value 1 for hsiz4 is there right away and there's no zeroes so I know that my data are correct. 

This process is a lot more useful when the data are not correct/what you expected and you want to see what's going on in those observations specifically that's led this to be the case. Of course it's much easier to type `tab hsiz4 hsizgp` to confirm the complete overlap between hsiz4 and hsizgp==4. Next let's check "bpsystol" and "bpdiast" in the variables pane within the data browser to display them (quick aside, blood pressure is two numbers, one systolic when the heart pushes blood through the arteries, and diastolic when it's just ready to begin the next push) systolic should always be greater than diastolic (unless there had been an error in the data entry -- most likely where the numbers were put in in reverse order). To check this in the browser we could sort the data by systolic pressure and then in reverse order by diastolic and scan down to see any incongruities. There's someone in line 874 with this sort that is either in bad shape or has a data entry error so lets investigate. In the variables pane check off hlthstat and heartatk (claims to be in good health, no heart attack history) we can further investigate by checking off age height weight, but we don't see anything that might lead to this here either -- not that we've examined this person, but that is an extremely narrow pulse pressure. 

![Checking data consistency with browser](./images/stataimg6a.PNG)

At this point, we might decide to drop this person from the data, consult with the facility that collected the data or leave the observation alone (the point here of course is to demonstrate the mechanism of finding things like this, not to tell you how to make a decision). An easier way to check for a transcription error (i.e. systolic pressure is lower than diastolic) is to type `count if bpdiast>bpsystol`. Stata returns zero so we're okay on that front. If we wanted to do the narrow/wide pulse pressure check we could generate a new variable `gen ppress=bpsystol-bpdiast` sort the data by our new variable `sort ppress` then list our first and last 20-25 obs `list ppress in 1/20` and `list ppress in -20/l`. Doing this, I can see that while I was concerned about the pulse pressure of 4, I should have been way more concerned about the very top values. Someone with a blood pressure of 300/120 should have "hlthstat"=deceased, not "hlthstat"=fair -- an obvious error in the data (anyone over 180 would normally be considered to be in hypertensive crisis). While you may not know much about blood pressure, when it comes to your data you will have some ideas of consistency checks you can do to make sure that you've ruled out large errors/inconsistency in your data.


For those who aren't going to watch the video and are just skimming pieces that they already have a grasp on, I will emphasize here that RDC data generally do not contain missing values coded as "." or "" as would be typical in a Stata file because missing data can be missing because it was a "valid skip" eg. we wouldn't ask someone born in Canada what year they immigrated to Canada generally appear in the data with some variation of the number 8 repeated. Similarly, did not answer appears as some variation of the number 9. What this means is that commands you might use to work with the data (like "if !missing" or "if variable==.") will not work properly. Check out the `mvdecode` video in the Varia section to learn how to deal with this.


#### Using .do files

- [Creating a do file](https://www.youtube.com/watch?v=bWj_-fQ9I9I)

Note that you don't need to code along here. Though, after watching this video you should either go back to "exploring data" and create a .do file to execute all or a selection of the commands, or, going forward, try to work from inside a .do file rather than using the command line interface.

Recall also what Alan showed us in the command modifiers video. If I want to execute a single line or selection of lines from a do file I can highlight the **contiguous** piece of code I want to run (i.e. I can't highlight several sections of code that are separated by other code) and press "Execute (do)" to execute. If you want to run several sections without executing the intervening code you will have to highlight and run them one after the other.

Finally, an expanded view of comments. You can and should add comments to your file to explain what you're doing. Most importantly you should provide a description at the top of your file to explain what it does. Stata also allows "bookmarks" within do files ![Bookmark](./images/stataimg7.PNG) to make navigating larger files easier. The leftmost button adds/deletes a bookmark, and the arrows allow you to navigate up to the previous bookmark or down to the next one. 

What is also possible within .do files is to run another .do file. You can do this by typing `do "*location of file.do*"`. This is a very useful feature since you can avoid using one large .do file to do your entire analysis and approach one problem per .do file. You should make use of either this or the bookmarks feature to organize your work.

#### Basic data cleaning & recoding

- [Generate, egenerate & recode](https://www.youtube.com/watch?v=npFshVxo9aA) | [Sample data to be used with the video](./sample_data/generate_egenerate_recode.dta)

There are tons of examples of generate and replace both in absolute and conditional forms in the "stfile-create datasets.do" program used to make up the data for the videos using pooled GSS.
	
When you get a little better at programming in Stata, you can get a little bit more efficient with things. The video above is super clear, and I'm glad that generate and recode are explained and demonstrated in a way that is very clear to someone new, but, generating dummy variables is easier using logical functions. Let's look at Alan's last example of creating dummy variables for interactions of race & sex, but suppose instead of wanting to create one variable, I want to create 6 dummy variables with the naming convention race_sex. I can specify the following:
	
> `generate black_female=0`
>
> `generate black_male=0`
> 
> `generate white_female=0`
> 
> `generate white_male=0`
> 
> `generate other_female=0`
> 
> `generate other_male=0`
> 
> `replace black_female=1 if race==2 & sex==2`
> 
> `replace black_male=1 if race==2 & sex==1`
>
> (and so on....)

But there's a better way to create dummy variables. I can create my variable using a logical function inside parentheses - like so:

> `generate black_female=(sex==2 & race==2)`
>
> `generate black_male=(sex==1 & race==2)`
> 
> `generate white_female=(sex==2 & race==1)`
> 
> `generate white_male=(sex==1 & race==1)`
> 
> `generate other_female=(sex==2 & race==3)`
> 
> `generate other_male=(sex==1 & race==3)`

Stata evaluates logical condition(s) to either 1 (if the condition(s) is true) or 0 (if the condition(s) is false). So for the first line, `generate black_female=(sex==2 & race==2)` evaluates to 1 iff (if and only if) both race and sex are equal to 2 (corresponding to black females in the data) the other conditions similarly evaluate to 1 when the race and sex are what we specify to correspond to the variables race and sex. Otherwise, they come out to zero which is what we want for our dummy variables. While we generally shouldn't be concerned with the number of lines of code, clarity to someone looking at the code is of critical importance, and the second way makes it much more obvious what the information contained in the new variables is. Note, however, that logical functions can get you in trouble! To see this let's play out what happens if sex were missing for an individual with race==2 observation what would happen?
	
Looking carefully at the first two lines where race is equal to 2, we should think through what will be returned by the logical test in parentheses. The first line will evaluate to zero (because sex isn't equal to 2), the second will also evaluate to zero (because sex isn't equal to 1 either). So this individual, whom we know is black and who must have a sex isn't labeled with any of the dummy variables. Stata doesn't know what you're trying to do and so **won't exclude** any of these observations from tabulations or other commands because they aren't missing. If you think there are, or may be, missing data then you would need to find a way to fix these new variables you've created - this is easily done with loops or by adding a condition to your statements. Of course this aside was purely pedagogical because, as Alan noted in the video, `egen` has a nice command to do what you want in terms of combining variables, and the `by` function allows you to treat the values of this new variable separately for any computation you might do.
	
Now let's take another common problem that comes up sometimes. Likert scales are very common in opinion or subjective questions. These are questions with a statement like "I believe that kittens are cuter than puppies" the response might look like "strongly agree", "agree", "unsure", "disagree", "strongly disagree", and to make working with them and storing datasets easier we almost always assign them values 1,2,3,4,5. A common thing to have to do is to flip the ordering of the values. This can be useful when you want to make the analysis easier to follow for the reader (for example - you might have a second statement like "I believe sugar gliders are cuter than kittens"). If you're trying to extract someone's predilection to be pro-kitten, you need to flip one of the two scales. You could do this using `recode`, but that's a lot of typing and you can do it much simpler by applying a function:
> 
>`gen puppy_better_kitten=(kitten_better_puppy-6)*(-1)`
>
Many problems like this exist in data cleaning and it can be helpful to think about how to efficiently approach problems. Note that I would make a new variable here rather than replace because the labels on the variables would be wrong if the original values were replaced (remember that you're changing the original data, but not the glasses that transform what you see).
	
Most of the time, if you need to modify the contents of your data you'll use generate, recode or replace. Generate takes most arguments; you can write the functions out by hand sometimes if they're simple mathematical expressions like adding or multiplying variables, you can use logical functions, or use other pre-canned functions. I can't really teach you every single function you might need, but remember the search function in Stata. There are a few things that you might find yourself using in `egen`. `egen` does the weird stuff - a common example is `tag()`. The `tag()` command is really useful when you're working with data where there's more than one observation per unit (country, or ID number or whatever). If you want to do something like take an average for your sample in a dataset where you have multiple observations per unit, you wouldn't want to do that for all observations, so you might create a tag variable to flag one observation and then use that variable condition to constrain your other commands. So if I were to figure out the average age of the countries in my panel I might do:
>
> `egen tagvar=tag(country)`
>
> `summarize founding_date if tagvar==1`
>
Be careful doing something like this for information that is *time varying* as the tagged observation will depend on the sort order of the data. Founding year is obviously not something that changes for any individual country, but if you wanted to know the average GDP of a country in the panel you should instead specify a condition on year:
>
> `summarize gdp if year==2004`
>
Of course this answer will be wrong if you have two observations for the same country in your data. To reiterate a point made earlier. If you don't know your data well, Stata will not stop you from making a mistake (recall that you can use `isid` on combinations of variables).

- [Encode](https://www.youtube.com/watch?v=ZRWHjdIZyxo)
	
Encode is very useful when you wind up with data that are stored as strings, BUT, be aware of the full set of values that are stored and make sure you've appropriately dealt with typos and the inherent messiness of strings in data as they usually exists in raw form. In the example Chuck provides in the video, if someone had made a data entry mistake and typed "balck" instead of "black", the numeric value for balck would be different of that for black. 

# Mini Assignment #1
By now you have some familiarity with Stata. Test your knowledge of the concepts introduced so far by completing the following operations:

1. Load in the "lifeexp" data used in the data browser example.
2. What type of variable is the "safewater" variable? What do you think Safewater represents?
3. Use a command to list the first five countries in the dataset
4. In a single line of code, determine whether any two countries have the same value for popgrowth.
5. Create a new variable that is a numeric representation of the information in the "country" variable
6. Using the mathematical function `round` (which has a helpfile!) create a new variable that is the life expectancy rounded to the nearest five.
7. Test your creativity: Using the helpfile for the function `floor` create a new variable that indicates the life expectancy to the decade in life in which life expectancy falls (note this is different than rounding because a country with life expectancy 79 has a life expectancy in the 70's.)
8. Explain in your own words the differences between the functions `ceil()`, `floor()`, `int()`, and `round()` in Stata. 
9. Challenge yourself: Create a variable that represents the deviation of the life expectancy in a given country from the median life expectancy for that region (ex. if a country had a life expectancy of 67 and the region's median life expectancy were 74 the value should be (-7)).
10. Save the modified dataset, empty your environment, then load the data back in, but only the numeric version of country which you created in 4 and any of the life-expectancy variable.

All set? [See the answers](YT link), and see the .do file and .smcl file in the solutions section.

#### Combining datasets

- [Merge](https://www.youtube.com/watch?v=niGZBRyyDuY)

This one is a good thing to have seen done, but as I joked about reshape in the intro, merge is one of those things that you will probably need to use the helpfile or the dialog box for (though copy the resulting code into your .do file) because, generally, you will want to use different master-files and using files, and specify the n:n differently depending on what you're doing, you'll often want to use the keep command, update command, nogen etc... and for how seldom this is done, it's too much to remember. 

Working with large datasets in Stata is difficult because it loads everything into memory to work and so for large administrative files it can take forever for a merge to work. The implication is that if you must use Stata to merge data you should think carefully about how to minimize the amount of data to be loaded into memory.
	
To understand the discussion that will happen next you need to understand which data are the "master" data and which ones are the "using" data. In the syntax: `merge 1:1 using "datafile.dta"`
	
It's somewhat easy to understand that the using data, are the data that appear in the merge command i.e. "datafile.dta". The master data is **whatever dataset that's in Stata *before* the merge command is issued**.
	
Another thing to know about merge is what happens with variables that share the same name. Suppose I have a variable called "age" and I collect data from the same individuals across time and now want to merge all the info I collected into one dataset to analyze. If my dataset has a value saved in a variable called "age" at each point. The *default* behaviour for Stata is to not change "age" from the "master" file to the value contained in the "using" file (i.e. ignore all variables in "using" that exist in "master"). What you can do is use the `update` option. If you only specify `update` merge will only change values that are missing in "master" and update them to whatever they are in "using". If you specify `update replace` it will be the opposite of `update`. The value of "age" in the merged dataset will be what is in the "using" data unless the data are missing in "using" in which case "age" will be whatever it was in "master". It may not be obvious, but from an analytical perspective, this is a **terrible** thing to do with a variable like "age" because age increases with time, and if data are collected over time then someone's "age" in the masterfile does not mean the same thing as "age" in the using file. You may know what you need to subtract from the second wave to determine age in the first wave (or you may not), but clearly *any* of the default behaviours of the merge command are probably not a good idea for a variable containing "age" that was collected across time. So what to do in this case? Well there are several options, and here I'm going to introduce a feature of Stata that you haven't seen.   
	
A lot of the time, before you're merging, you'll need to align your data. Your key (the variable that you match between the two datasets) might have different names, an issue like that we described with "age" might happen, and so on.... I present the following -- more concrete -- scenario: the master data contains an ID variable called "ID_no" a variable for health status and the variable age. The using data has a matching ID variable (but it's called "IDno" instead because of a careless data entry team), an age variable, and a second measure for health status. You can get the two data-frames [here](./sample_data/merge_sample_Health_survey_wave_1.dta) and [here](./sample_data/merge_sample_Health_survey_wave_2.dta). Let's code this up together. First open both datasets and see what's going on.
	
Let's start with a naive merge to see how things work:
	
> `use merge_sample_Health_survey_wave_2.dta, clear`
>
> `merge 1:1 IDno using merge_sample_Health_survey_wave_1.dta`

	
Immediately we get an error "IDno not found". But we see it in our wave 2 data. When we open wave 1 we see that ID_no is the variable name (hopefully that didn't surprise you, I said I was going to do that). Now we need a way to deal with this incongruity. The simplest way for small datasets is to make use of "tempfiles". "Tempfiles" are a minimum two command process that lets us create a temporary file that we can modify and store, but that Stata won't save permanently (as part of its operations Stata deletes temporary files and variables after every execution). It also helps avoid having to make the difficult choice of either overwriting our raw data to change the variable name, or else saving another copy of it to use in our merge file (neither of which is a good idea in most cases).
	
Start by opening our wave 1 data and renaming that pesky ID variable:

> `use merge_sample_Health_survey_wave1.dta, clear`

> `rename ID_no IDno`

now we create and save the tempfile as follows:

> `tempfile tempdata`
>
> `save 'tempdata', replace`

we can now use our wave 2 data and merge just like we did before except this time instead of the using data being "merge_sample_Health_survey_wave_1.dta" we'll call for our modified version --"\`tempdata'"-- instead. Note that the quote marks used to refer to temporary variables look a bit unusual. The left tick is he one next to the 1 key on a standard keyboard, the right tick is the one that shares a key with \" in a standard keyboard. In your do-file editor, your text will turn green if you've done it properly (regular quoted text in normal quotes " " is red). Note that because the left tick is used in markdown formatting, I can't provide the code in the usual code block format for the second command.

> `use merge_sample_Health_survey_wave_2.dta, clear`
>
> merge 1:1 IDno using \`tempdata'

Importantly, tempfiles can *only* be used in one command -- as I said in introducing them -- they are deleted by Stata after every execution. You can't use a tempfile in the command window or with the point & click interface, only within a ".do" file. If you wanted to do this from the command window you would have to either modify your raw data or save a copy (or use a preserve command -- see the "Varia" section of this training).

Once you have copied the appropriate code into a ".do file" and run it, you'll see an issue. Namely, we only have three variables. If you looked carefully at the original data you'd see hlthstatus variable in wave 2 was a *relative* health variable asking respondents whether their health is better or worse than before, and in wave 1 was an absolute level "very good" to "very poor". "age" is also not the same between waves and we might want to account for that especially since age is missing sometimes. We'll deal with this when we rename the ID_no variables to make things clear.

> `use merge_sample_Health_survey_wave1.dta, clear`
>
> `rename hlthstatus hlthstatus_w1`
>
> `rename age age_w1`
>
> `rename ID_no IDno`
>
> `tempfile tempdata`
>
> save \`tempdata', replace
>
> `use merge_sample_Health_survey_wave_2.dta, clear`
>
> `rename hlthstatus hlthstatus_w2`
>
> `rename age age_w2`
>
> merge 1:1 IDno using \`tempdata'

Now if you look at your data, you can see the full merged dataset. We could now make an *informed* decision about what to do with age if we wanted (without checking the ".do file" I used to create the fake data, can you figure out how to use Stata to determine how much time passed between waves? Verify that it is indeed 6 months as I intended) and we've retained both the initial health in w1 and the relative health in w2. You'll also see that Stata has created a variable called "\_merge" which is 3 in all observations. This tells us that the key (IDno) was present in both the using and master data. There is a lot to learn about the merge command, but this is enough for this introduction. To practice what you've learned try using the `append` command. Add the dataset "append_sample_Health_survey.dta" ([download here](./sample_data/append_sample_health_survey.dta)) to the wave 2 dataset. Check the helpfile for `append` to get started.

#### The Reshape

In data, there are two ways to present multiple observations for a given category. The first would be to add a new row to your data, tagging it with a variable to indicate the category and then dropping in all the values for the variables of interest -- this would add length to your dataset. The other is to add more columns to your data with the new observations (like we did in the merge section when we did hlthstatus_wave1 and hlthstatus_wave2) which makes your data wider. Both are valid ways to store the information, however it may not be possible to analyze and combine the way you want to in one format of the other (for example, Stata is good at row-totals, but not so good at summing up values of a variable across rows tagged with a given value of a second variable.) Because of this, Stata has a reshape function to convert a dataset between long (where a new unit-level observation adds new rows making the data longer), and wide (where a new unit-level observation adds new columns making the data wider). 

The syntax for the reshape command is a pain, but it is a powerful tool. It relies on an `i()` variable representing the unit of observation, and a `j()` variable representing the sub-observations. In our health-survey example in merge (the data for which we continue with in this section), `i()` would be the ID_no variable, and `j()` the wave (either 1 or 2). In practice, of course, you might have many waves, and your data will contain a lot more information than age and healthstatus. Some of this extra information might be constant for the individual and some of it might be time-varying. 

Example rows of data in wide format

|IDno|hlthstatus_w1| hlthstatus_w2| age_w1| age_w2|_merge|
|--|--|--|--|--|--|
|10 |poor|better|18|.|Matched(3)
|11| good|about the same|29|30|Matched(3)

Now we `reshape` the example rows of data to be long: `reshape long hlthstatus_w age_w, i(IDno) j(wave)`

the *j* variable is a new variable to be generated. You'll see when you run this code that each ID number has 2 rows. A row for wave 1 and a row for wave 2.

|IDno| wave| hlthstatus| age | _merge|
|--|--|--|--|--|
|10 |1|worse|18|Matched(3)|
|10| 2|better|.|Matched(3)|
|11| 1|better|29|Matched(3)|
|11|2|about the same|30|Matched(3)|

You should immediately notice a problem with your new dataframe. The long reshape worked perfectly, but now our hlthstatus variable says worse for wave one when we know it should be "poor". Because the variables aren't measuring the same thing they aren't really collapse-able. We know our data, we could strip the label from the variable and make sure that we are always conditioning on wave when we do anything, or we could recode the healthstatus variable in wave 2 to a different set of numbers and redefine the label. But, for all these workarounds, this data makes more sense not to be "long". Let's go back to wide. The easiest way to do this is to type `reshape wide` by default Stata will "undo" the last reshape long command... unless.

"cannot reshape data containing variable _merge". 

This is an idiosyncracy, you can have non-time-varying information in a reshape. Let's `rename _merge non_time_varying_info`, and then `reshape wide`. We'll see that wave, the j-variable, gets dropped and the proper number added back to the "hlthstatus" and "age" variables. Our non-time varying information gets put in as a single entry. If this variable were not constant for all IDno, Stata would tell us and refuse to reshape. If we weren't working from a previous reshape, the syntax for this wide reshape would look like `reshape wide hlthstatus age, i(IDno) j(wave)`. Unfortunately, our proper labels did not survive the un-reshape and have to be re-loaded.

Again, it's worth knowing that this feature exists and how it works, but you will likely need the help-file to implement it.				

# Statistics

#### Tables of summary statistics
- [Table & Tabstat (for Stata versions before 17.0)](https://www.youtube.com/watch?v=7H09uQxh23A). [Get sample data](link)
- [Tables (for Stata verisons 17 and up)](https://youtu.be/7H09uQxh23A?si=IO8qAITSTSZ2LQd8&t=121). You should still review the tabstat command from the previous video [it starts at 2:00](https://youtu.be/7H09uQxh23A?si=Z3n-sabXaRWzNzy2&t=120) 

Tables and Tabstat are useful ways to compute statistical information. This is one of the few situations where there has been major changes to the way that the syntax works between versions of Stata. IF you like the old version, you can open Stata and type `version 16` (or any other version number for that matter) and Stata will act like it did in that version. Obviously, you can't make an old version of Stata act like a newer one. 

#### Creating a dataset of summary values
- [Collapse](https://www.youtube.com/watch?v=nczbK9LJJv0)

Collapse is an interesting feature especially as it relates to the RDC. Obviously it's useful to be able to compute aggregated data from microdata, but more interestingly, in combination with `export excel`, it's really easy to create and store unweighted copies of your summary statistics outputs as well as unweighted counts (as Sebastian demonstrated with the (count) count=varname functionality) for the purposes of vetting reviews. There are other ways to do this, but this is very fast to code up for small datasets. To do multiple passes of collapse, you can use "tempfiles" as demonstrated in the merge section to save->collapse->restore your data.

#### Data visualization
- [Creating a visualization](https://www.youtube.com/watch?v=PWFZ_-eQd8I)

Visualizations are a critical way to present data. Stata provides satisfactory visualizations, but my preference is for visualizations to be created in Python/R and Stata can natively include Python script inside a .do file. (See the "Other skills" section for PyStata). Make sure you see our python training for data visualization help in Python. You should unashamedly make use of the help files and point-and-click interface to make visualizations with Stata.

#### Correlations & Regressions
- [Correlations](https://www.youtube.com/watch?v=uivL8QmlBXA) (pairwise Pearson correlation & Spearman correlation) 

- [Ordinary least squares regression](https://www.youtube.com/watch?v=HafqFSB9x70) | OLS section starts at 2min, but make sure you load in the nlsw88 data to follow along.

As you are likely aware, when doing regression analyses, researchers often make use of dummy variables and interaction terms to allow the intercepts and slopes of the model to vary depending on various other dimensions of the data being modeled. You can add these dimensions to your model by either creating a new variable that reflects the interaction or series of dummy variables you want to model, or by using some shortcuts in the software. For example, suppose I have a variable for race (you do, if you're still working with the nlsw88 from the video). If I just enter the race variable, Stata will try to estimate the model as if the difference between 1 (white) and 2 (black) was the same as going from 2(black) to 3(other). You instead need to model it with dummy variables. You can generate a series of dummy variables like you did in the encode/gen/replace video -- OR -- you can use the "i." function. Let's demonstrate both (including a shortcut for the first).

Using the nlsw88 data, try the following:

`tab race, generate(dvar)`

You'll see that Stata has automatically created three new dummy variables for each value of "race" (called dvar1 dvar2 dvar3 -- note that the number appended to the variable name I specified represents the stored value of the data and not the labelled value) and these can be used within your regression. Let's now use these variables in a regression of hours worked.

`reg hours dvar*`

*note that the asterisk tells Stata to use every variable that starts with "dvar"* If you are careful with the `generate` option in `tabulate, generate` to avoid looking like any of the other variables in your data you can use the asterisk to speed things up.

Next let's learn the "i." function. For a variable that is categorical or dichotomous, you can use i.*varname* to estimate a coefficient for every value of the variable.

`reg hours i.race`

Now what you'll see (if your Stata is the same as mine), the omitted race category was different (my dvar* left black out of the model, but i.race will -as is the default- leave out the first category). Let's see how to change it:

`reg hours ib2.race` 

Now we get the same coefficient values! Of course if you want to change the default for the dvar* method you would need to write out the categories you do want in the model. Finally, if you're trying to estimate a model with no constant to get an estimate for each category you cannot use the i. formulation because Stata will always omit one of the categories.

Next is interactions. Here, there's no option for tab *varlist*, gen (that will fail) so you would have to go about creating the full set of dummy variables in another way. However, we have the # operator:

`reg hours race#c_city`

will estimate the model only for the interaction terms, while

`reg hours race##c_city` 

will estimate coefficients for the interactions, but also the individual elements of the interaction. You can also add another variable with # or ## to specify a three-way interaction.

To close off, you will need to know how to specify a continuous variable in an interaction. To do this we just use `c.` as in the following example with age:

`reg hours race##c.age`

Note that this is not meant to be in any way a primer on statistics. There are many options around specification of the structure of the error term that you need to look into in the documentation depending on the model you are using. There are also many types of model specifications in Stata (ex. Poisson regression, Ordered probit etc.). The syntax for these models is generally very similar to OLS with interacted terms working the same way.

# Varia

Note that for these concepts, I don't provide any sample data. You should be familiar enough now with data types and the sample datasets to make use of whichever in-built dataframe you want to work through these exercises.

#### Strings

Working in Stata allows the use of both older functions for dealing with strings, but also *regular expressions* which are software agnostic expressions of patterns in text. If you're learning, you should unequivocally use regular expressions to work with strings. I will be posting a tutorial on regular expressions soon, but you can make use of the helpfiles for `regexm` `regexr`, and, less commonly, `regexs` for now to try to understand these functions.

The remaining string functions that are useful are `subinstr` and `substr` ([see video](https://www.youtube.com/watch?v=YEO8Y-1misw), but also the `egen` subfunction `ends` - which would have been a faster way to approach the pricing problem in the video and is a very handy way to deal with strings when the formats are very standardized. 

#### Other concepts/skills to master
- Treatment of "missing" values
	+ [Handling missing data in Stata](https://www.youtube.com/watch?v=XcBFxaHmoas)  |  Recall that in Statistics Canada data, *missingness is **not** represented by "."*. This video focuses more on the fact that missing is treated like positive infinity.
	+ [mvdecode](https://www.youtube.com/watch?v=6HV2773-dVM) | Used to re-assign numeric data to represent missing.
	+ Note that nothing here deals with the statistical problems of missingness. Videos on multiple imputation in Stata are available and I *strongly* recommend investigating this.
- [Using stored values](https://www.youtube.com/watch?v=RjepW4WXb_E)
	+ Professor Caetano does a good job explaining the process and advantages of using stored information (and as a bonus shows you the predict command). One advantage for RDC users is that if your .do file does not contain any data or information derived from the data, it can easily be released from the RDC where it may be useful during the publication process (either in reproducibility packages, online appendices, or simply for your own reference.)
	+ If you need to keep a piece of information after you use a new command you'll need to assign the information you need to retain to a local or global (see "Macros" for guidance on this feature.)
- [Using loops](https://www.youtube.com/watch?v=eNFqzNBSBF8)

	Loops (or a comparable feature or mechanism) is prevalent across statistical programming applications. It allows you to save space by doing the  same operation on multiple items. It can be hard to know when to use loops, but knowing about them is useful for when you find yourself using the same command over and over in sequence or when you need to work through a series of numbers.  
- Macros
	+ [Locals](https://www.youtube.com/watch?v=Ovk0CiTxMRI)  At this point you should be comfortable enough that you can use any dataset to follow along with this video -- no sample dataframe is provided. (note that Alan's expanded loop (@ time 22:00) contains an error - in loop 3 the text highlighted in yellow should be "hrs1" not "wordsum") but since you've seen loops just prior to this, you might either use this as review or skip the loops portion of the video.
	+ Global (create my own... short description and how to use)
- [Temporary files/preserve & restore](https://www.youtube.com/watch?v=B0joqUDJDwg)  | We covered this briefly in "Merge", but preserve/restore will be new
- [Log files](https://www.youtube.com/watch?v=3N9l9i5HyaE)
	+ A log-file was used to provide responses to cases where I had to make up data to go along with another video.
	+ Note that these can be useful while you're working on a project in the RDC, but that having them released from the RDC is a lot to ask of your analyst (and yourself) and is not likely to be useful within the lab. Note that as Steffen suggests you should generally use the `replace` option, otherwise your log file will get quite long.
	+ Steffen presents nicely, but he gets something wrong. He puts ".txt" and ".smcl" in his filename which should not be done. Try instead to just specify a filename (like this `log using  "mylogfile", replace`) then after replace specify one additional option: `text` to save it as a text file, or `smcl` to save it in the default Stata markup language. If you need to share your log with someone who doesn't have access to Stata, you should use the text option because it will be interpretable on any text editor software. Otherwise, the SMCL format is much easier on the eyes.
- Using survey data: 
	- [Establishing the survey design](https://www.youtube.com/watch?v=XYjWCL7IEKU)
	
		For RDC data, we don't observe the Sampling Unit Strata, or any FPCs that might be used to adjust the data. All this information is contained in the survey weight variable in the RDC. You also need to be careful of survey design, because some datasets contain multiple weights (sometimes hh_wt or pers_wt, but generally one weight to represent the household and one to represent the person within the household). Basically, if the person is randomly selected from within the members of a household, a person from a larger household is less likely to receive the survey and this needs to be corrected for when the research question is about the *individual*. When the research question is about the *household* we don't want to make that last correction and should use the household weight. As an example, if you're trying to understand the effect of a recent birth on household income, you should use the household weight. If you instead were trying to understand the impact of a recent birth on fathers' or mothers' incomes specifically, you should use the individual weight.
	- [Analyzing the data](https://www.youtube.com/watch?v=0DRXnoR-Q1c)
	
		You will see that once the data are survey set, the commands that accept a survey weight work by prepending the abbreviation `svy` to the command. This works with a large number of commands (in my copy of Stata 17 my survey analysis options are even more numerous than Chuck's in Stata 13 in the video.) If you don't want to survey set your data, you can usually specify a sampling weight within the command (`pweight=*varname*`). Check the helpfiles to see how this is implemented. Note that this requires understanding the differences between a sampling design/post-collection stratification weight (ipw -- inverse probability weights) and a frequency weight. Without overcommitting myself, you will almost exclusively encounter p-weights almost within the RDC. Do not try to use some other kind of weight if the command you want doesn't allow inverse probability weights. Additionally, many Statistics Canada datasets come with bootstrap replication weights that allow for bootstrap inference. These are generally strongly recommended if not required when they can be computed. When survey setting the data, you provide the bootstrap weight variables to Stata from within the weights tab:
		
		![Weights tab](./images/stataimg8.PNG)
		
		In your subsequent commands when you specify the method by which to calculate statistical information as "bootstrap", Stata will make use of the provided weights in its calculations.
	- [Using python within Stata](https://www.youtube.com/watch?v=P3_ioddIVKk) | [Download slides here](./resources/IntroToPython.zip)

		This content assumes some familiarity with Python, and you should skip it if you don't want to learn, or don't already know, Python. When integrated, this duo allows you to take advantage of the effecient and user-friendly data manipulation tools in Stata and the beautiful visualizations and complex and customizable methodological tools available within Python. While the slides provided give an overview of downloading and installing Python, you won't need to do this part within the RDC.
		
	- [Change directories](https://www.youtube.com/watch?v=7OnadOj1kG8)
	
		Here, Steffen shows you how to set your working directory, but doesn't show you how to work within a set of sub-folders that you want to use to organize your project. You might have a "data" folder, a "results" folder and so on. How to do this is as follows. Use the `cd` command and point it at the top level folder that makes sense and from which you might sensibly refer to other places. You add a new folder by typing one or more subfolders as you would progress through them in a folder browser. For example, in my folder for this project, my top-level directory is "F:\CRDCN\Software\stata_training". Suppose I'm working in a do file and I want to save data. I can simply `save "sample_data\citydata.dta"`. There are two schools of thought around working directories with some users preferring instead to use global macros to pre-specify a bunch of different directories. Unless the folder structure itself is strange in some way, or the way they will be shared between different users very unusual, I see no reason to specify a variety of these rather than using cd to set the working directory to a sensible place from which to refer to the rest. You should also be aware that you can move up and down within your folder structure using periods so using my current directory ("F:\CRDCN\Software\stata_training") if I wanted to save a dataset in the "Software" folder, I could type `save "..\mydata.dta"` and the data would save there.
	

# Stata Assignment:

1. Load in the pop2000 data that is packaged with Stata
2. Create a set of new variables to capture non-white population. Total, as well as male and female.
3. Create a new (numeric) variable to categorize the population into 3 bins: Child (<=19), Working age (20-64) and Retirement age(65+) - label this new variable
4. Create a table of values using your new variables (white/non-white) by the child/working age/retirement-age variable. Your table should have the data in each category as well as the total for all age categories by white/non-white.
5. Create a population plot for three different racial categories graphing the *share* of the total population in each age bin. e.g. Indian population \<5 is 8.65\% of the total Indian population, 5 to 9 are 9.70\% of the total Indian population and so on...
6. Load the voter data that is packaged with Stata
7. Fix the numbering for the candidate variable so that they are in alphabetical order and take the values 1,2,3. Ensure the variable is labeled properly.
8. What kind of variable is each variable in this dataset, what do you think they measure? Put a variable label on the three unlabeled variables.
9. Create a variable that ranks the size of each voting block, i.e. the largest block of voters should be rank 1 and the smallest block rank 15. 
10. How much does the democratic vote share fall for each increase in an income bin? Use a regression model to calculate the amount.
Now assume that you don't want to specify that the relationship between income and vote share is linear. Create a model that doesn't assume the relationships is uniform. 

# Other resources

 - The [Statalist Forum](https://www.statalist.org/forums/) is a cornucopia of asked and answered questions about Stata. If you don't come across your issue, you can create an account and ask!
 - The [Stata Manuals](https://www.stata.com/features/documentation/) provide a topic-by-topic overview of core and advanced functionality in Stata. These are the places that the help files in Stata link to, but you'll be able to search the whole document if you access it directly.
 - The [Stata Journal](https://www.stata-journal.com/) hosts papers for researchers that have written new commands in Stata. If you're using particular and specific user-written Stata plugins you should try to cite these papers.  