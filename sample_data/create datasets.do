//This file was used to create all the sample data for the CRDCN stata training modules. Other required datasets should be created here also
/* ~~~~~~~~~~~~~*****************~~~~~~~~~~~~~~~~~~
Syntax to create the data available for download for Command modifiers video
Info about dataset that can be gleaned from the video.
numeric variable educ (years of education 0-20)
numeric variable year incl(1972-2012)
numeric variable race incl (1,2,3 no missing)
numeric variable age incl(.)
numeric variable sex incl(1,2 - no missing)
*/
clear
cd "F:/CRDCN/Software/stata_training/sample_data"
set seed 80

*set the number of observations to 57,061 just like in the dataframe
set obs 57061

*generate a years of education variable of max 20, mean of 12
gen educ=rpoisson(12)
replace educ=20 if educ>20

*generate a uniformly distributed year variable
gen year=runiform(1972,2012)
replace year=round(year)

*generate a race variable with no missing data
gen race=runiform(1,3)
replace race=round(race)
label define r1 1"white" 2"black" 3"other"
label values race r1

*generate an age variable with some missing data (5%)
gen age=rnormal(42, 12)
replace age=0 if age<=0

gen missing=runiform(0,1)
replace age=. if missing<=0.05
drop missing

*generate a sex variable (labeled) with some missing data (5%)
gen sex=runiform(1,2)
replace sex=round(sex)

label define s1 1"male" 2"female"
label values sex s1

**save the current dataset state to use as example material for the conditional statements video. Use the old format to maximize compatibility for older stata versions
keep sex race year educ age

saveold "command_modifiers.dta", replace

/*  ~~~~~~~~~~~~~~~~~~~~~***********************~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Data for the data cleaning video is based on the same survey. Extend the variable list to include the new stuff then save an update
*/
label define conflab 1"a great deal" 2"only some" 3"hardly any"
gen m1=runiform(0,1)
gen m2=runiform(0,1)
gen m3=runiform(0,1)
gen m4=runiform(0,1)
gen m5=runiform(0,1)

**conbus - business confidence**
gen busrand=runiform(0,1)
gen conbus=(busrand<=0.24)
replace conbus=2 if busrand<=.85 & conbus~=1
replace conbus=3 if conbus==0
replace conbus=. if m1>0.95
label values conbus conflab

**coneduc - education**
gen educrand=runiform(1,3)
gen coneduc=round(educrand)
label values coneduc conflab
replace coneduc=. if m2>0.95

**conlabor - labor market**
gen labrand=runiform(0,1)
gen conlabor=(labrand<0.4)
replace conlabor=2 if labrand<=0.9 & conlabor~=1
replace conlabor=3 if conlabor==0
label values conlabor conflab
replace conlabor=. if m3>0.95

**conpress - press**
gen prrand=runiform(0,1)
gen conpress=(prrand<0.27)
replace conpress=2 if prrand<=0.73 & conpress~=1
replace conpress=3 if conpress==0
label values conpress conflab
replace conpress=. if m4>0.95

**conlegis - legislative branch**
gen lgrand=runiform(0,1)
gen conlegis=(lgrand<0.4)
replace conlegis=2 if lgrand<0.65 & conlegis~=1
replace conlegis=3 if conlegis==0
label values conlegis conflab
replace conlegis=. if m5>0.95

keep sex race year educ age conlegis conpress conlabor coneduc conbus

**save the current dataset state to use as example material for the generate egenerate recode video. Use the old format to maximize compatibility for older stata versions

saveold "generate_egenerate_recode.dta", replace

/*~~~~~~~~~~~~~~~~~~~~~~~*************************~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
In-text example for the "merge" section. This creates a made up dataframe of 100 individuals who are hypothetically surveyed about their health status twice.
*/
clear
tempfile t1
set obs 100

//--create the second dataset first

gen IDno=(_n)
label variable IDno "Unique Identifier"
gen hlthstatus=runiform(1,6)
replace hlthstatus=int(hlthstatus)
gen age=rnormal(32,6)
replace age=round(age)

label variable hlthstatus "How would you describe your health relative to when we asked 6 months ago?"
label define hl2 1"much better" 2"better" 3"about the same" 4"worse" 5"much worse"
label values hlthstatus hl2
label variable age "What is your current age?"

save `t1', replace

drop hlthstatus
gen agemod=runiform()
replace age=age-1 if agemod<0.50
drop agemod
gen hlthstatus=runiform(1,6)
replace hlthstatus=int(hlthstatus)

label define hl1 1"very good" 2"good" 3"fair" 4"poor" 5"very poor"
label values hlthstatus hl1
label variable hlthstatus "How would you describe your overall health?"

gen m1=runiform()
replace age=. if m1<=0.075
drop m1

rename IDno ID_no
order ID_no
compress
saveold "merge_sample_Health_survey_wave_1.dta", replace

use `t1',replace

gen m2=runiform()
replace age=. if m2<=0.075
drop m2
compress
order IDno
saveold "merge_sample_Health_survey_wave_2.dta", replace

**Now create a dataset to use in an `append' activity.
drop IDno
gen IDno=_n+105
rename IDno ID
rename hlthstatus health
order ID
saveold "append_sample_Health_survey.dta", replace



//~~~~~~~~~~~Summary Statistics TabStat & Table~~~~~~~~~~~~~~~~~~~//
clear
//Used chatGPT to give me a namelist :)
import delimited "nameslist.csv", clear 
gen ID=_n

egen FirstName=ends(v1), tr head
egen LastName=ends(v1), tr tail
drop v1
rename v2 City
rename v3 State
rename v4 Major
gen Country="USA"
gen Age=round(rnormal(20,3))
gen SAT=round(rnormal(1175,99))
gen Heightin=round(rnormal(67,5))
gen Averagescoregrade=round(rnormal(74,6))
replace Averagescoregrade=100 if Averagescoregrade>100
gen Newspaperreadershiptimesweek=int(runiform(0,5))
gen G1=runiform(0,1)
gen Gender="Female" if G1<0.5
replace Gender="Male" if Gender==""
drop G1

saveold "table_and_tabstat_for_Stata16.dta", replace

//Slight recode for the ACS2014 data used in the collapse video

rename Newspaperreadershiptimesweek race
replace race=race+1
rename Averagescoregrade educ
gen empstat=(runiform()<0.2)+1
encode Gender, generate(sex)
label values sex .
rename Age age
gen state1=round(runiform(1,2))
gen state="IN" if state1==1
replace state="IL" if state1~=1
gen marst=round(runiform(1,7))
gen wage=rnormal(43000, 12000)
egen educcategory1=cut(educ), group(5)
gen educcategory="NODIPLOMA" if educcategory1==0
replace educcategory="HIGHSCHOOL" if educcategory1==1
replace educcategory="ASSOCIATE" if educcategory1==2
replace educcategory="BACHELOR" if educcategory1==3
replace educcategory="GRADUATE" if educcategory1==4

keep state sex age marst race educ empstat wage educcategory
saveold "collapse.dta", replace

