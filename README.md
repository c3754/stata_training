# Stata Training

## Getting started

Welcome to the CRDCN training module on Stata. In this module you will learn the basics of working in Stata and this material is set-up for a novice user. If you've never even opened Stata before, you're in the right place. If you've only ever used the menus to interact with the software and copied/pasted your output you will be a much more capable user by the time you're finished. While you don't need to know Stata to use this module, basic familiarity with statistics will be useful. 

If you have feedback or suggestions for new content, please feel free to contact me at grant.gibson[at]crdcn.ca.


## How to "do" the training

This training makes extensive use of youtube content from a variety of presenters. Each video is accompanied by some additional context and/or information.

The Stata content contains two folders of supporting material for the watchlist. One is called sample data and contains a small dataset to work through the videos that don't use the built-in sample data. The other is called solutions, and contains answers to exercise *and* it contains the workthrough of the videos
since the data I made up obviously can't correspond to the data that the video presenters are using. It also contains a file that generates all the data, you don't need to use it, but I provide it for reference. Finally, there is a folder called images, which contains the images required to compile the module. You shouldn't need to access the material in this folder.

The last thing to know is that at the top of the files include a line:
'Point line X at the correct place for your system'
cd F:/CRDCN/Software...

You will need to modify the line beginning cd to match the location in your system where the material is stored. If there are spaces in your file path, you will have to put the path in quotes " "

When you're ready to start just click on "training material.md" above and you'll be ready to go.

## See something amiss or amissing?

Do you see an error or something that you feel should be changed or added? Feel free to drop me a line at grant.gibson@crdcn.ca with the suggestion. Please be as specific as possible. If you're interested in contributing I'm happy to add collaborators to this project. Any improvement to the repository is a welcome one.

## License

This content is partially original and partially drawn from the freely available public work of others. Where I or the Canadian Research Data Centre Network via McMaster University could exercise a copyright claim, the work is licensed under CC-BY-SA-NC. Put simply, you may use, transform, repurpose, present, share or do essentially anything **non-commercial** with the work provided that you A) provide proper attribution to the Canadian Research Data Centre Network and me (Grant Gibson), and B) share your content based on this work under the same license terms. Full details are available in the license file in the repository.
