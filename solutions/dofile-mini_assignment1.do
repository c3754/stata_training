**Solutions to mini assignment 1
capture log close
clear
log using "solution-mini_assignment1", smcl replace
//1.
sysuse lifeexp

//2.
*Safewater is a 'byte' variable. This means that it is an integer with a max value of 100 (min value of -100). It's not well labelled, the variable label "safe water" doesn't give us much. In the Properties frame though, we can see in the notes though: "Access to safe water, % of population". It is good that is is documented this way because in the Data notes, the data source resolves to a 404 (the world bank has moved the website so the original source is lost). A byte variable is ideal for storing a percentage like this where there's no decimals, but if there were more precision in the data it would need to be stored in a different format. Without the label, while this variable does indeed have all the hallmarks of a percentage it could easily have been defined multiple ways (for example a scaled index to the #1 ranked country).

//3.
list country in 1/5

//4. There are two options here: `isid', and `duplicates' (you haven't been shown duplicates, but it's a sensible way to understand the problem)

duplicates list popgrowth
//or
capture : isid popgrowth
/*note that were the output not suppressed by "capture", this would return 
"popgrowth does not uniquely identify the observations"

notice the difference here. isid is a useful way to confirm that a variable 
or combination of variables has no duplicates if there is a duplicate it will 
throw an error and stop your code. If there are no duplicates it will be passed 
over with no issue. If you have a piece of your program that will be wrong, but 
won't cause an error, if there are duplicates you can precede it with an isid 
line to ensure that the error doesn't appear in your calculations. Notice in my 
.do file I had to put "capture" before my isid line so that the do file would 
continue to run after the error. `duplicates list`' can be more helpful to
understanding your data because it shows you which lines the duplicates are in.
*/
//5.
encode country, gen(countryID)
/*note that the opposite operation *decode* is a bit less useful, taking labeled
numeric data and creates a string
*/
//6. 
generate lexp_5=round(lexp, 5)

//7.
gen lexp_dec=lexp/10
replace lexp_dec=floor(lexp_dec)
replace lexp_dec=lexp_dec*10

//8.
/* Round is fairly self evident as it returns the datapoint rounded to the 
nearest x. We used this function for question 5 to round to the nearest 5, but 
had we specified a 10 after the comma it would have rounded to the nearest 10, 
or if we specified 100 to the nearest 100 (where all the values would be 100). 
Ceil() can be though of as being like round-up, but only for integers (we can't 
specify 5 here for example). For numbers less than zero, this means the result 
will get closer to zero, and for numbers greater than zero the result will get 
further from zero. Floor() is exactly the opposite, it is like round-down, but 
only for integers with numbers less than zero getting further from zero, and 
numbers larger than zero getting closer to zero. Int() behaves like floor() for 
numbers greater than zero (rounding them towards zero), and like ceil() for 
numbers less than zero (rounding them towards zero). The only behaviour that 
isn't coded is the last one, rounding things away from zero ie. behaving like 
ceil() for numbers greater than zero, and floor() for numbers less than zero. 
These would have to be done in two separate operations with an if statement 
(although the reason that it's not coded is because there's not a ton of 
applications for this specific way of rounding). If you need to use floor/ceil 
to round to something other than the nearest integer, you may need to do 
something like divide by 10 (as we did in 6, then multiply again after 
rounding.) Or you might need to combine rounding with some of the other 
functions.
*/
//9.
bys region: egen temp=median(lexp)
gen lexp_deviation=temp-lexp
drop temp

//10.
save modified_lifeexp, replace
clear
use countryID lexp using "modified_lifeexp.dta"

** And that's it! 
log close