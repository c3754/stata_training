** get solutions for the command modifiers video
cap log close
clear
***** Point Line 5 to the proper directory in your system*****
cd F:\CRDCN\Software\stata_training
**************************************************************
log using ".\solutions\solution-command_modifiers_solution", smcl replace
use .\sample_data\command_modifiers


summarize educ if year==2012
summarize educ if year==2012 & race==1
summarize educ if year==2012 & (race==1 | race==2)
summarize educ if year ==2012 & race<3
summarize educ if year==2012
summarize educ if year==2012 & ~missing(race,age,sex)


list age in 10
list age in 1/10
list age in 10/20
list age in 57040/l
list age in f/10
list age in -10/l


by sex, sort: summarize educ if year==1972
bysort sex: summarize educ if year==1972
bysort race: summarize educ if year==1972
bysort race: summarize educ if year==2012

log close