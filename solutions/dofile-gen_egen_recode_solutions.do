**solutions for the command modifiers video
cap log close
********Point line 4 to the proper directory in your system*********** 
cd F:\CRDCN\Software\stata_training
**********************************************************************
log using ".\solutions\solution-generate_egenerate_recode", smcl replace
use ".\sample_data\generate_egenerate_recode", clear

tab conbus
tab conbus, nolab

generate conidx1=conbus+coneduc+conlabor+conpress+conlegis
tab conidx1

//you might have noticed that Alan summed all five things again, I'm far too lazy for that, though not too lazy to write a much longer comment pointing it out.
generate conidx2=conidx1/5
tab conidx2

egen cntnmiss=rownonmiss(conbus coneduc conlabor conpress conlegis)
tab cntnmiss //note that because in the real data missingness is highly dependend on other values being missing.
egen conidx3=rowmean(conbus coneduc conlabor conpress conlegis) if cntnmiss>=4
drop cntnmiss


//section 2
tab race
tab race, nolabel

generate racedum1=.
replace racedum1=0 if race==2
replace racedum=1 if race==1

tab racedum1

lab def racelab1 0"black" 1"white"
label values racedum1 racelab1
label variable racedum1 "Respondent race (0=black/1=white)"

tab racedum1

//NB this is a really bad way to define a dummy variable (it should be white and take the value 1 if yes zero if no -- BUT, this variable is missing "." for race==other)

generate racedum2=.
replace racedum2=0 if race>=2 & race<.
replace racedum2=1 if race==1

label define racelab2 0"non-white" 1"white"
label values racedum2 racelab2
label variable racedum2 "Respondent race (0=non-white 1=white)"

tab racedum2

//faster way to generate the variable: generate racedum2=(race==1) if race~=.    - the portion in the brackets will evaluate to 1 if the expression is true and 0 if it isn't true. - Try it.

//for the rounding, you can see where I applied rounding to generate the dummy variables when creating this dataframe.

*** using -recode- to create new measures *IMPORTANT* RECODE OVERWRITES YOUR DATA IN MEMORY IF YOU DON"T SPECIFY THE GENERATE OPTION. IF YOU MESS UP OR OVERWRITE YOU HAVE TO START OVER.

recode race (1=1 "white") (2/3=0 "nonwhite") (else=.), generate(racedum3)
tab racedum3


#delimit ;
recode age  (18/19=1 "18-19")
			(20/29=2 "20-29")
			(30/39=3 "30-39")
			(40/49=4 "40-49")
			(50/59=5 "50-59")
			(60/69=6 "60-69")
			(70/79=7 "70-79")
			(80/89=8 "80-89") (else=.), gen(agecat);
#delimit create
tab agecat

histogram agecat, discrete xlab(1(1)8,valuelabel)
@timestamp 23:47

log close