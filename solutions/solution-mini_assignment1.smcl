{smcl}
{com}{sf}{ul off}{txt}{.-}
      name:  {res}<unnamed>
       {txt}log:  {res}S:\CRDCN\stata_training\solutions\solution-mini_assignment1.smcl
  {txt}log type:  {res}smcl
 {txt}opened on:  {res}13 Mar 2024, 09:18:48
{txt}
{com}. //1.
. sysuse lifeexp
{txt}(Life expectancy, 1998)

{com}. 
. //2.
. *Safewater is a 'byte' variable. This means that it is an integer with a max value of 100 (min value of -100). It's not well labelled, the variable label "safe water" doesn't give us much. In the Properties frame though, we can see in the notes though: "Access to safe water, % of population". It is good that is is documented this way because in the Data notes, the data source resolves to a 404 (the world bank has moved the website so the original source is lost). A byte variable is ideal for storing a percentage like this where there's no decimals, but if there were more precision in the data it would need to be stored in a different format. Without the label, while this variable does indeed have all the hallmarks of a percentage it could easily have been defined multiple ways (for example a scaled index to the #1 ranked country).
. 
. //3.
. list country in 1/5
{txt}
     {c TLC}{hline 12}{c TRC}
     {c |} {res}   country {txt}{c |}
     {c LT}{hline 12}{c RT}
  1. {c |} {res}   Albania {txt}{c |}
  2. {c |} {res}   Armenia {txt}{c |}
  3. {c |} {res}   Austria {txt}{c |}
  4. {c |} {res}Azerbaijan {txt}{c |}
  5. {c |} {res}   Belarus {txt}{c |}
     {c BLC}{hline 12}{c BRC}

{com}. 
. //4. There are two options here: `isid', and `duplicates' (you haven't been shown duplicates, but it's a sensible way to understand the problem)
. 
. duplicates list popgrowth

{p 0 4}{txt}Duplicates in terms of {res} popgrowth{p_end}
{txt}
  {c TLC}{hline 7}{c -}{hline 5}{c -}{hline 10}{c TRC}
  {c |} {res}Group   Obs   popgro~h {txt}{c |}
  {c LT}{hline 7}{c -}{hline 5}{c -}{hline 10}{c RT}
  {c |} {res}    1     9        -.1 {txt}{c |}
  {c |} {res}    1    12        -.1 {txt}{c |}
  {c |} {res}    2    10          0 {txt}{c |}
  {c |} {res}    2    41          0 {txt}{c |}
  {c |} {res}    3    20         .1 {txt}{c |}
  {c LT}{hline 7}{c -}{hline 5}{c -}{hline 10}{c RT}
  {c |} {res}    3    30         .1 {txt}{c |}
  {c |} {res}    3    31         .1 {txt}{c |}
  {c |} {res}    4     6         .2 {txt}{c |}
  {c |} {res}    4    11         .2 {txt}{c |}
  {c |} {res}    4    34         .2 {txt}{c |}
  {c LT}{hline 7}{c -}{hline 5}{c -}{hline 10}{c RT}
  {c |} {res}    5     5         .3 {txt}{c |}
  {c |} {res}    5    16         .3 {txt}{c |}
  {c |} {res}    5    21         .3 {txt}{c |}
  {c |} {res}    5    25         .3 {txt}{c |}
  {c |} {res}    5    32         .3 {txt}{c |}
  {c LT}{hline 7}{c -}{hline 5}{c -}{hline 10}{c RT}
  {c |} {res}    5    35         .3 {txt}{c |}
  {c |} {res}    5    42         .3 {txt}{c |}
  {c |} {res}    6     3         .4 {txt}{c |}
  {c |} {res}    6    13         .4 {txt}{c |}
  {c |} {res}    6    15         .4 {txt}{c |}
  {c LT}{hline 7}{c -}{hline 5}{c -}{hline 10}{c RT}
  {c |} {res}    6    26         .4 {txt}{c |}
  {c |} {res}    6    28         .4 {txt}{c |}
  {c |} {res}    6    33         .4 {txt}{c |}
  {c |} {res}    6    36         .4 {txt}{c |}
  {c |} {res}    7    14         .5 {txt}{c |}
  {c LT}{hline 7}{c -}{hline 5}{c -}{hline 10}{c RT}
  {c |} {res}    7    17         .5 {txt}{c |}
  {c |} {res}    7    19         .5 {txt}{c |}
  {c |} {res}    7    24         .5 {txt}{c |}
  {c |} {res}    7    29         .5 {txt}{c |}
  {c |} {res}    7    44         .5 {txt}{c |}
  {c LT}{hline 7}{c -}{hline 5}{c -}{hline 10}{c RT}
  {c |} {res}    8    37         .7 {txt}{c |}
  {c |} {res}    8    46         .7 {txt}{c |}
  {c |} {res}    8    67         .7 {txt}{c |}
  {c |} {res}    9    52          1 {txt}{c |}
  {c |} {res}    9    56          1 {txt}{c |}
  {c LT}{hline 7}{c -}{hline 5}{c -}{hline 10}{c RT}
  {c |} {res}    9    57          1 {txt}{c |}
  {c |} {res}    9    58          1 {txt}{c |}
  {c |} {res}   10     1        1.2 {txt}{c |}
  {c |} {res}   10    45        1.2 {txt}{c |}
  {c |} {res}   11     4        1.4 {txt}{c |}
  {c LT}{hline 7}{c -}{hline 5}{c -}{hline 10}{c RT}
  {c |} {res}   11    22        1.4 {txt}{c |}
  {c |} {res}   11    59        1.4 {txt}{c |}
  {c |} {res}   12    53        1.9 {txt}{c |}
  {c |} {res}   12    55        1.9 {txt}{c |}
  {c |} {res}   13    39          2 {txt}{c |}
  {c LT}{hline 7}{c -}{hline 5}{c -}{hline 10}{c RT}
  {c |} {res}   13    50          2 {txt}{c |}
  {c |} {res}   13    63          2 {txt}{c |}
  {c |} {res}   13    66          2 {txt}{c |}
  {c |} {res}   14    38        2.4 {txt}{c |}
  {c |} {res}   14    64        2.4 {txt}{c |}
  {c LT}{hline 7}{c -}{hline 5}{c -}{hline 10}{c RT}
  {c |} {res}   14    68        2.4 {txt}{c |}
  {c |} {res}   15    40        2.8 {txt}{c |}
  {c |} {res}   15    54        2.8 {txt}{c |}
  {c BLC}{hline 7}{c -}{hline 5}{c -}{hline 10}{c BRC}

{com}. //or
. capture : isid popgrowth
{txt}
{com}. 
. * notice the difference here. isid is a useful way to confirm that a variable or combination of variables has no duplicates if there is a duplicate it will throw an error and stop your code. If there are no duplicates it will be passed over with no issue. If you have a piece of your program that will be wrong, but not cause an error, if there are duplicates you can precede it with an isid line to ensure that the error doesn't appear in your calculations. Notice in my .do file I had to put "capture" before my isid line so that the do file would continue to run after the error. `duplicates list`' can be more helpful to understanding your data because it shows you which lines the duplicates are in.
. 
. //5.
. encode country, gen(countryID)
{txt}
{com}. *note that the opposite operation *decode* is a bit less useful, but takes labeled numeric data and creates a string
. 
. //6. 
. generate lexp_5=round(lexp, 5)
{txt}
{com}. 
. //7.
. gen lexp_dec=lexp/10
{txt}
{com}. replace lexp_dec=floor(lexp_dec)
{txt}(63 real changes made)

{com}. replace lexp_dec=lexp_dec*10
{txt}(68 real changes made)

{com}. 
. //8.
. * Round is fairly self evident as it returns the datapoint rounded to the nearest x. We used this function for question 5 to round to the nearest 5, but had we specified a 10 after the comma it would have rounded to the nearest 10, or if we specified 100 to the nearest 100 (where all the values would be 100). Ceil() can be though of as being like round-up, but only for integers (we can't specify 5 here for example). For numbers less than zero, this means the result will get closer to zero, and for numbers greater than zero the result will get further from zero. Floor() is exactly the opposite, it is like round-down, but only for integers with numbers less than zero getting further from zero, and numbers larger than zero getting closer to zero. Int() behaves like floor() for numbers greater than zero (rounding them towards zero), and like ceil() for numbers less than zero (rounding them towards zero). The only behaviour that isn't coded is the last one, rounding things away from zero ie. behaving like ceil() for numbers greater than zero, and floor() for numbers less than zero. These would have to be done in two separate operations with an if statement (although the reason that it's not coded is because there's not a ton of applications for this specific way of rounding). If you need to use floor/ceil to round to something other than the nearest integer, you may need to do something like divide by 10 (as we did in 6, then multiply again after rounding.) Or you might need to combine rounding with some of the other functions.
. 
. //9.
. bys region: egen temp=median(lexp)
{txt}
{com}. *note that if the data weren't pre-sorted we'd have to use bys region instead to pre-sort the data.
. gen lexp_deviation=temp-lexp
{txt}
{com}. drop temp
{txt}
{com}. 
. //10.
. save modified_lifeexp, replace
{txt}{p 0 4 2}
(file {bf}
modified_lifeexp.dta{rm}
not found)
{p_end}
{p 0 4 2}
file {bf}
modified_lifeexp.dta{rm}
saved
{p_end}

{com}. clear
{txt}
{com}. use countryID lexp using "modified_lifeexp.dta"
{txt}(Life expectancy, 1998)

{com}. 
. ** And that's it! 
. log close
      {txt}name:  {res}<unnamed>
       {txt}log:  {res}S:\CRDCN\stata_training\solutions\solution-mini_assignment1.smcl
  {txt}log type:  {res}smcl
 {txt}closed on:  {res}13 Mar 2024, 09:18:48
{txt}{.-}
{smcl}
{txt}{sf}{ul off}